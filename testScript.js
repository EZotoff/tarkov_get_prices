const {Builder, By, Key, util, promise} = require("selenium-webdriver");
const chrome = require('selenium-webdriver/chrome');

const screen = {
  width: 640,
  height: 480
};

const driver = new Builder()
.forBrowser('chrome')
.setChromeOptions(new chrome.Options().headless().windowSize(screen))
.build();

async function example() {
  try {
    await driver.get("http://google.com");
    await driver.getTitle().then(function(title) {
      console.log("The title is: " + title)
    });
    await driver.quit();
  }

  catch(err){
    handleFailure(err, driver)
  }
}

function handleFailure(err, driver) {
  console.error('Something went wrong!\n', err.stack, '\n');
  driver.quit();
}

example(baseUrl + "meds");