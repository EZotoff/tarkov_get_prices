// use 'node getPrices.js' to run
"use strict";
const {Browser, Builder, By} = require("selenium-webdriver");
const {Options, ServiceBuilder} = require('selenium-webdriver/chrome');
const fs = require('fs');
const uploadSheet = require('./uploadSheet');

var herokuFlag = true;
var chromedriverPath;
var chromeBin;

const chromeOptions = new Options();
chromeOptions.addArguments("--headless");
chromeOptions.addArguments("--no-sandbox");
chromeOptions.addArguments("--disable-gpu");
chromeOptions.addArguments("--proxy-server='direct://'");
chromeOptions.addArguments("--proxy-bypass-list=*");
// chromeOptions.addArguments('--remote-debugging-port=9222');
// chromeOptions.addArguments('--disable-dev-shm-usage');
// chromeOptions.addArguments("--window-size=920,2080");

if (process.env.GOOGLE_CHROME_SHIM != null) {
  chromedriverPath = process.env.GOOGLE_CHROME_SHIM;
} else {
  chromedriverPath = './chromedriver.exe';
}
if (process.env.GOOGLE_CHROME_SHIM != null) {
  console.log(process.env.GOOGLE_CHROME_SHIM);
  chromeBin = process.env.GOOGLE_CHROME_SHIM;
  chromeOptions.setChromeBinaryPath(chromeBin);

  // chromeBin = '/app/.apt/usr/bin/google-chrome';
  // chromeBin = '/app/.apt/opt/google/chrome/chrome';
}

let serviceBuilder = new ServiceBuilder(chromedriverPath);

const DEBUG = false;

const baseUrl = "https://tarkov-market.com/ru/tag/";

async function getTagData(tag) {
  try {

    var driver = new Builder()
                        .forBrowser(Browser.CHROME)
                        .setChromeOptions(chromeOptions)
                        .setChromeService(serviceBuilder)
                        .build();

    await driver.get(getUrl(tag));
    console.log(`Started driver for processing tag ${tag}`);
    await sleep(2000);

    var table_xpath = '//div[@id="__layout"]/div/div[1]/div/div[3]/table';

    var table = await findEl('xpath', table_xpath, driver);

    var table_body = await findEl('tag', 'tbody', table);

    // Get table height
    var table_height_prev = 1;
    var table_height = 0;

    // === Get data from tag page ===
    var tagData = [];

    while (table_height_prev != table_height) {
      // Store old table hight
      table_height_prev = await getAttr(table, 'clientHeight');
      if (DEBUG == true) {console.log(`Table height prev: ${table_height_prev}, Table height: ${table_height}`);}

      // Scroll down to end
      await driver.executeScript("window.scrollTo(0, document.body.scrollHeight);").then(await sleep(2000));

      // Get new table height
      table_height = await getAttr(table, 'clientHeight');
      if (DEBUG == true) {console.log(`Table height prev: ${table_height_prev}, Table height: ${table_height}`);}
    }

    // Get number of rows in table
    var table_rows = await table_body.findElements(By.tagName('tr')).then(els => els);
    if (DEBUG == true) {console.log(`Number of rows found: ${table_rows.length}`);}

    // Get data for each item and merge together
    const itemTasks = table_rows.map(getItemData);
    var itemResults = await Promise.all(itemTasks);
    var tagData = [];
    itemResults.forEach(function(itemData) {
      tagData.push(itemData);
    });

    console.log(`Found ${tagData.length} items for tag ${tag}`);

    return tagData;
  }

  catch(err){
    handleFailure(err, driver);
  }

  finally {
    await driver.close();
  }
}

function getUrl(tag) {return baseUrl + tag};

async function findEl(by, locator, source) {
  switch (by) {
    case 'xpath':
      return await source.findElement(By.xpath(locator)).then(el => el);
    case 'tag':
      return await source.findElement(By.tagName(locator)).then(el => el);
    default:
      break;
  }
}

async function getAttr(el, attrName){
  return await el.getAttribute(attrName).then(val => val);
}

async function getItemData(table_row) {
  // Get img element
  var item_img_xpath = "td/a/figure/img";
  var img = await findEl('xpath', item_img_xpath, table_row);

  // Get img source
  var img_source = await getAttr(img, 'src');
  if (DEBUG == true) {console.log(`img_source: ${img_source}`);}

  // Get name (from img alt)
  var item_name = await getAttr(img, 'alt');
  if (DEBUG == true) {console.log(`item_name: ${item_name}`);}

  // Get link to item page (from img href)
  var item_link = await getAttr(await findEl('xpath', "td/a", table_row), 'href');
  if (DEBUG == true) {console.log(`item_link: ${item_link}`);}

  // Get id (from item page link)
  var item_id = item_link.split(/\//).slice(-1)[0];
  if (DEBUG == true) {console.log(`item_id: ${item_id}`);}

  // Get price avg_24h
  var avg_24h_text = await getAttr(await findEl('xpath', "td[3]/span", table_row), 'textContent');
  avg_24h_text = avg_24h_text.replace(/[^\d.]/g, '');
  if (DEBUG == true) {console.log(`avg_24h_text: ${avg_24h_text}`);}

  // Get number of slots item takes
  var price_per_slot_el = await table_row.findElements(By.xpath("td[3]/span[2]")).then(els => els);

  if (price_per_slot_el.length == 0) {
    var item_slots = 1;
  } else {
    var price_per_slot = await getAttr(price_per_slot_el[0], 'textContent');
    price_per_slot = price_per_slot.replace(/[^\d.]/g, '');
    var item_slots = Math.round(avg_24h_text / price_per_slot);
  }

  // Get price change 24h
  var chg_24h_text = await getAttr(await findEl('xpath', "td[4]", table_row), 'textContent');
  chg_24h_text = chg_24h_text.replace(/[+]/, "");
  if (DEBUG == true) {console.log(`chg_24h_text: ${chg_24h_text}`);}

  // Get price change 7d
  var chg_7d_text = await getAttr(await findEl('xpath', "td[5]/div/span", table_row), 'textContent');
  chg_7d_text = chg_7d_text.replace(/[+]/, "");
  if (DEBUG == true) {console.log(`chg_7d_text: ${chg_7d_text}`);}

  // Record item data
  return `${item_name}, ${item_id}, ${item_slots}, ${avg_24h_text}, ${chg_24h_text}, ${chg_7d_text}, ${item_link}, ${img_source}`;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function handleFailure(err, driver) {
  console.error('Something went wrong!\n', err.stack, '\n');
  driver.quit();
}

function writeData(fileName, data) {
  // Write results to CSV
  console.log(`Saving ${data.length} items to CSV`);
  fs.writeFileSync(fileName, data.join('\n'));
}

  async function getAll() {
    var fileName = "./item_data/item_list.csv";
    // Get tags from a comma-de;imeted list
    var tags = fs.readFileSync('tags.csv', 'utf-8', (err, data) => {
      if (err) throw err;
      return data;
    })
    .split(',');

    // Get data for each tag and merge together
    console.log(`Getting data for tags: ${tags}`);
    const tasks = tags.map(getTagData);
    var results = await Promise.all(tasks);
    var allData = [];
    results.forEach(tagData => {
      tagData.forEach(item => {
        allData.push(item);
    })});

	  // Write data to CSV
    writeData(fileName, allData);

    // Close all webdriver instances
    var driver = new Builder(chromedriverPath)
                          .forBrowser('chrome')
                          .setChromeOptions(chromeOptions)
                          .build();
    await driver.quit();

    console.log('Finished, uploading to Sheets');

    var uploadRes = uploadSheet.uploadData();
    console.log(uploadRes);
  }

getAll();