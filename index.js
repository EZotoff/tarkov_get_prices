const express = require("express");
const app = express();
// const getPrices = require("./getPrices.js");
// function run() { require("./getPrices.js"); }

app.get("/", (req, res) => {
  res.sendFile(__dirname + '/index.html');
  // res.send( getPrices.getAll() );
});

app.post('/submit-click', function (req, res) {
  res.send( require("./getPrices.js") );
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});